#include "Fraction.h"
#include <iostream>
#include <istream>
#include <ostream>
using namespace std;

Fraction::Fraction()
{
  m_num = 1;
  m_denom = 1;
}

float Fraction::GetDecimal() const
{
	return static_cast<double>(m_num) / m_denom;
}

Fraction Fraction::CommonDenominatorize( const Fraction& other ) const
{
	if (m_denom == other.m_denom) { return *this; }
	Fraction result;
	int commonDenom = m_denom * other.m_denom;
	result.m_num = m_num * other.m_denom;
	result.m_denom = commonDenom;
	return result;
}

Fraction& Fraction::operator=( const Fraction& other )
{
	if (this == &other) { return *this; }
	m_num = other.m_num;
	m_denom = other.m_denom;
	return *this;
}

Fraction operator+( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	Fraction result;
	result.m_num = commonLeft.m_num + commonRight.m_num;
	result.m_denom = commonRight.m_denom;

	return result;
}

Fraction operator-( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	Fraction result;
	result.m_num = commonLeft.m_num - commonRight.m_num;
	result.m_denom = commonRight.m_denom;

	return result;
}

Fraction operator*( const Fraction& left, const Fraction& right )
{
	Fraction result;
	result.m_denom = left.m_denom * right.m_denom;
	result.m_num = left.m_num * right.m_num;

	return result;
}

Fraction operator/( const Fraction& left, const Fraction& right )
{
	Fraction result;
	result.m_num = left.m_num * right.m_denom;
	result.m_denom = left.m_denom * right.m_num;

	return result;
}

bool operator==( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	return(commonLeft.m_num == commonRight.m_num && commonLeft.m_denom == commonRight.m_denom);

}

bool operator!=( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	return(!(commonLeft.m_num == commonRight.m_num && commonLeft.m_denom == commonRight.m_denom));
}

bool operator<=( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	return(commonLeft.m_num <= commonRight.m_num);
}

bool operator>=( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	return(commonLeft.m_num >= commonRight.m_num);
}

bool operator<( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	return(commonLeft.m_num < commonRight.m_num);
}

bool operator>( const Fraction& left, const Fraction& right )
{
	Fraction commonLeft = left.CommonDenominatorize(right);
	Fraction commonRight = right.CommonDenominatorize(left);

	return(commonLeft.m_num > commonRight.m_num);
}

ostream& operator<<( ostream& out, const Fraction& fraction )
{
	out << fraction.m_num << "/" << fraction.m_denom;

	return out;
}

istream& operator>>( istream& in, Fraction& fraction )
{
	in >> fraction.m_num >> fraction.m_denom;

	return in;
}

